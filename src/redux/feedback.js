import * as ActionTypes from './ActionTypes';


export const Feedback = (state = {
    errMess: null,
    feedbacks: []
}, action ) => {
    switch (action.type) {
        case ActionTypes.ADD_FEEDBACK:
            var feedback = action.payload;
            console.log("feedback: ", feedback);
            return {...state, feedbacks: state.feedbacks.concat(feedback)};
        default:
            return state;
    }
}