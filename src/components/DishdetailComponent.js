import React, { Component } from 'react';
import { Card, CardImg, CardText, CardBody, CardTitle, Breadcrumb, BreadcrumbItem, Button, Modal, ModalHeader, ModalBody, Row, Label, Col} from 'reactstrap';
import { Link } from 'react-router-dom';
import { Control, LocalForm, Errors } from 'react-redux-form';
import { Loading } from './LoadingComponent';
import { baseUrl } from '../shared/baseUrl';

import { FadeTransform, Fade, Stagger } from 'react-animation-components';



    function RenderDish({dish}){
            return(
                <div className="col-sm-12 col-md-5 m-1" >
                <FadeTransform
                in
                transformProps={{
                    exitTransform: 'scale(0.5) translateY(-50%)'
                }}>
                    <Card>
                        <CardImg width="100%" src={baseUrl + dish.image} alt={dish.name} />
                        <CardBody>
                            <CardTitle>{dish.name}</CardTitle>
                            <CardText>{dish.description}</CardText>
                        </CardBody>
                    </Card>
                 </FadeTransform>
                </div>
               
            );
    }

   function RenderComments({comments, postComment, dishId}){
        // fecha opciones
        const options = {month: 'short', day: '2-digit',year: 'numeric' };
        
        if(comments != null){
                return(
                 <div className="col-sm-12 col-md-5 m-1">
                     <h4>Comments</h4>
                        <ul className="list-unstyled">
                        <Stagger in>
                           {comments.map((comment) => {
                                return(
                                    <Fade in>
                                    <li key={comment.id}>
                                        <p>{comment.comment}</p>
                                        <p>-- {comment.author}, {new Date(comment.date).toLocaleDateString("en-US",options)}</p>
                                    </li>
                                    </Fade>
                                );
                  })}
                        </Stagger>              {/* <li>--{comme.author}, <span> {new Intl.DateTimeFormat('en-US', { year: 'numeric', month: 'short', day: '2-digit'}).format(new Date(Date.parse(comme.date)))}</span></li> */}
                         </ul>
                         <CommentForm dishId={dishId} postComment={postComment}/>
                 </div>
                );
        }
        else {

            return(
                <div>

                </div>
            )
         
        }
    }

    const required = (val) => val && val.length;
    const maxLength = (len) => (val) => !(val) || (val.length <= len);
    const minLength = (len) => (val) => (val) && (val.length >= len);

    class CommentForm extends Component {
        constructor(props) {
            super(props);
            this.state = { 
                isModalOpen: false
             }
             this.toogleModal = this.toogleModal.bind(this);
        }

        toogleModal(){
            this.setState({
                isModalOpen: !this.state.isModalOpen
            })
        }

        //    evento para activar el formulario
        handleSummit(values) {
            this.props.postComment(this.props.dishId, values.rating, values.author, values.comment);
            // console.log("Current state is: " + JSON.stringify(values));
            // alert("Current state is: " + JSON.stringify(values));
            // event.preventDefault()
        }
        render() { 
            return ( 
                <div>
                <Button outline onClick={this.toogleModal}>
                    <span className="fa fa-pencil"></span> Summit Comment
                </Button>
                <Modal isOpen={this.state.isModalOpen}  toggle={this.toogleModal}>
                    <ModalHeader toggle={this.toogleModal}>Submit Comment</ModalHeader>
                    <ModalBody>
                    <LocalForm onSubmit={(values) => this.handleSummit(values)}>
                        <Row className="form-group">
                            <Col>
                              <Label htmlFor="rating">Rating</Label>
                                <Control.select  model=".rating"
                                name="rating"
                                className="form-control">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </Control.select>
                            </Col>
                        </Row>
                        <Row className="form-group">
                            <Col>
                                <Label htmlFor="author">Your Name</Label>
                                <Control.text model=".author" 
                                    type="text"
                                    id="author"
                                    name="author"
                                    placeholder="Your Name"
                                    className="form-control"
                                    validators={{
                                        required, minLength: minLength(3), maxLength: maxLength(15)
                                    }} 
                                />
                                <Errors 
                                    className="text-danger"
                                    model=".author"
                                    show="touched"
                                    messages={{
                                    required: 'Required',
                                    minLength: ' Must be greater than 2 characters',
                                    maxLength: ' Must be 15 characters or less'
                                    }}
                                />
                            </Col>
                        </Row>
                        <Row className="form-group">
                            <Col>
                                <Label htmlFor="comment">Comments</Label>
                                <Control.textarea model=".comment" 
                                    id="comment"
                                    name="comment"
                                    rows="6"
                                    className="form-control"
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Button type="submit" color="primary">
                                    Submit
                                </Button>
                            </Col>
                        </Row>
                    </LocalForm>
                    </ModalBody>
                </Modal>
               </div>
             );
        }
    }
     
  

    const DishDetail = (props) => { 
        if (props.isLoading) {
            return(
                <div className="container">
                    <div className="row">
                        <Loading />
                    </div>
                </div>
            );
        } 
        else if (props.errMess) {
            return(
                <div className="container">
                    <div className="row">
                        <h4>{props.errMess}</h4>
                    </div>
                </div>
            );
        }
        else if  (props.dish != null) 
        return ( 
            <div className="container">
                 <div className="row">
                     <Breadcrumb>
                        <BreadcrumbItem><Link to='/menu'>Menu</Link></BreadcrumbItem>
                        <BreadcrumbItem active>{props.dish.name}</BreadcrumbItem>
                    </Breadcrumb>
                        <div className="col-12">
                            <h3>{props.dish.name}</h3>
                            <hr />
                        </div>
                  </div>
                <div className="row">
                <RenderDish dish={props.dish}/>
                <RenderComments comments={props.comments} 
                postComment={props.postComment}
                dishId={props.dish.id}
                />
                {/* {this.renderDish(this.props.dish)}
                {this.renderComments(this.props.dish)} */}
                </div>
            </div>
         );
        else 
        return (
            <div></div>
        );
    }

 
export default DishDetail;